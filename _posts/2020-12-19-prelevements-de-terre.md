---
title: Prélèvements de terre
created: 2020-12-19
layout: post
---

Aujourd'hui, nous réalisons des prélèvements de terre afin d'en connaître la composition générale.

L'idée derrière cette "analyse" est de réussir à déterminer l'emplacement idéal pour les différents légumes.

## Prélèvements
Pour notre jardin, nous avons prélevé de la terre à sept endroits différents, verticalement, pour remplir des bocaux à moitié (il n'y avait pas de notion de profondeur sur nos différents "carrotages"). `8` et `9` n'ont pas été prélevés pour le moment, faute de bocaux.

![Repérages](/assets/images/20201219_carte_reperages.svg)

Les trous ont été repérés avec un bout de fer à béton coiffé d'un bouchon rouge numéroté pour ne pas se prendre les pieds dedans.

{% picture assets/images/20201219_142900.jpg alt="Prélèvement et balisage" %}

Une fois tous les prélèvements effectués, le double de volume d'eau à été ajouté dans les bocaux, et la terre a été remuée quelques minutes, afin de casser les mottes et les blocs de terre. Nous avons retiré les racines présentes, sans savoir si c'était pertinent ou non.

Les bocaux numérotés sont ensuite laissés à décanter quelques jours afin qu'un dépôt se fasse, mais en observation préliminaire, nous avons sur le terrain au moins trois compoositions différentes (terre marron, terre très sombre et un bocal entre les deux).

{% picture assets/images/20201219_145932.jpg alt="Bocaux en décantation" %}

## Lecture des résultats

Une fois les mélanges décantés, nous devrions être en mesure de _voir_ plusieurs strates, représentant le taux de sable, d'argile et de limon présents dans la terre. Cette lecture, bien expliquée dans le livre "Développer son potager en permaculture", par Nelly Pons (p.20) mériterait un outil pour la faciliter...

En attendant les résultats on va coder un petit quelque chose permettant de les lire.
