---
title: Naissance d'une idée
created: 2020-12-12
updated: 2020-12-17
layout: post
author: M.
---

Lassés de la ville et de ses sirènes, nous avons cette année cherché à nous en éloigner.
Il fallait trouver une maison avec un peu de terrain, à 25 minutes maximum de nos lieux de travail respectifs.

## L'existant

Après plusieurs visites et quelques concessions, nous avons trouvé quelque chose dans nos moyen, dont voici la carte :

![Terrain vide](/assets/images/20201216_carte_vide.svg)

Environ 1700m² de terrain contenant déjà quelques arbres, où nous allons nous mettre à jardiner. L'objectif est encore flou, mais il est question de résilience. Et pour apprendre, j'aime faire.

Après une recherche préliminaire lors de nos premières visites, il s'avère que nous avons

- cinq noyers
- un beau pied de vigne le long d'un mur en béton
- des pommiers
- un poirier
- un abricotier
- deux figuiers
- trois noisetiers
- quelques buissons
- un laurier (_sauce_, à priori)

En attendant la vente, nous avons commencé à réfléchir à notre futur jardin : quoi planter et où ?

Nous avons donc commencé à dessiner notre jardin, d'abord à partir de calques réalisés sur des vues aériennes, puis en utilisant un logiciel de dessin libre, [Inkscape](https://www.inkscape.org). Cet outil a tout pour du dessin de carte (du moins de manière non professionnelle) : du dessin vectoriel (_pas de pixels_), des _calques_, des formes duplicables et déplaçables. En quelques heures, nous sommes arrivés cette carte ci:

![Terrain complet](/assets/images/20201216_carte_arbres.svg)

En gros, nous avons remis les arbres et arbustes, colorés par genre, la taille représentant grosso-modo leur âge.  Les points représentent les zones qui d'après nous seront principalement ombragées.

## Le premier jet

On a rajouté une mare, des chemins, et on a essayé de caser le maximum de parcelles de 5x3 mètres environ, au plus proche de la sortie de la maison, pour voir ce que ça donnerait :

![Terrain complet](/assets/images/20201216_carte_parcelles.svg)

Bien entendu, pour une première année, le but n'est pas de cultiver autant, et pas sur un découpage aussi linéaire. Si nous arrivons à avoir une zone de culture dans la partie ombragée, ainsi qu'une autre dans la zone du milieu, ce serait déjà bien, pour un début...

## Naissance de Garden Party

En attendant la vente, nous trépignons. Les quelques travaux concernant la maison sont chiffrés, reste l'inconnue : le jardin. La carte est jolie mais ne permet pas de suivi ; c'est une carte...

Donc l'idée germe, au fil de nuits à me demander par où commencer, comment garder une trace de ce qui sera fait, de créer une application. Son but sera simple pour commencer : pouvoir re-créer la carte que nous avions imaginée et assurer un suivi de la vie de ces éléments, tout en planifiant les actions à mener : plant, taille, récolte, ...

Le confinement de novembre lié à la COVID19 a donné un peu de temps pour développer une application qui répond à ce cahier des charges.

C'est donc avec un code opensource (license MIT), que Garden Party voit le jour.

{% picture assets/images/20201216_gp_interface.png alt="Interface principale" %}

## Maturation et publication

Au cours des quelques semaines de développement initial, il a été proposé de pouvoir utiliser les cartes OpenStreeMap directement en fond de plan, en plus des images uploadées et c'est maintenant possible.
{% picture assets/images/20201216_gp_osm.png alt="Carte OSM" %}


De plus, il a fallu créer une bibliothèque de plantes, ce qui a mené à ces différents points:
- le genre des plantes ? (la _famille_ aurait peut-être été plus pertinente, à discuter... )
- les relation avec d'autres plantes ? (les tomates ont une bonne relation avec les œillets d'inde)
- une description ? (OK, on les a pompées sur Wikipedia, mais du coup, il fallait une _source_ pour l'indiquer)
{% picture assets/images/20201216_gp_librairie.png alt="Librairie" %}

Chaque plante "appartient" à un des niveaux que l'on retrouve dans les concepts de forêt-jardin (du sou-sol à la canopée), ce qui permet de filtrer l'affichage sur la carte.

On peut planter des éléments "seuls", comme des arbres, ou créer des parcelles contenant plusieurs éléments.

Il est possible d'ajouter des animaux sur les cartes, car oui, ~~ce sont des plantes~~ ils ont leur place dans les jardins. Bon, la bibliothèque en est exempte pour le moment.

Un élément (animal ou végétal) a une sorte de cycle de vie: _planifié_ (on veut/va en mettre), _mis en place_ (on l'a planté/mis en liberté) ou _retiré_ (le plant est mort, on l'a coupé, l'animal \*n'est plus là\*).
{% picture assets/images/20201216_gp_actions.png alt="Cycles de vie" %}

Chaque action est planifiable et pour les plus courantes, on les retrouve dans une liste de "trucs à faire"
{% picture assets/images/20201216_gp_todo.png alt="Liste de tâches" %}

Chaque élément a son historique d'actions.
{% picture assets/images/20201216_gp_historique.png alt="Historique" %}

Une vue d'ensemble affiche tous les différents éléments.
{% picture assets/images/20201216_gp_vue_d_ensemble.png alt="Vue d'ensemble" %}

### Techniquement parlant...

C'est une application Ruby on Rails (web, donc) avec, pour l'_application_, un frontend en VueJS et des cartes affichées grâce à OpenLayers. Pour l'administration, des vues "standard" Ruby on Rails.

Une API REST est disponible pour les utilisateurs inscrits (à voir s'il ne faudrait pas l'ouvrir en partie pour la librairie). Une documentation Swagger est dispo aussi, pour un petit confort non négligeable si vous voulez créer votre client.

Le code est testé, au moins sur le _backend_ (droits, retours d'API, comportement des actions, ...), et les tests pourraient être améliorés sur le _frontend_.

Le support multi-langue est implémenté; l'ensemble de l'application est traduite en français/anglais (mis à part les données : c'est en français)

Pour les devs Rails + Vue, il y a des trucs potentiellement intéressants: des tâches Rake pour générer les **stores VueX** et des simili-modèles en JS; des **_templates_ de _scaffolding_** modifiés pour les contrôleurs, vues d'administrations et tests de requête ; une gem qui **teste les retours API avec RSpec** et génère la doc Swagger (elle aurait bien besoin d'amour, cette gem) ; les traductions `vue-i18n` sont gérées par Rails \o/, ...

## Futur de Garden Party

Le projet est jeune mais a un potentiel d'évolution intéressant ; hormis le fait qu'il soit libre, quelques idées font partie de la feuille de route aujourd'hui, et seront développées si une communauté se forme autours de ce logiciel :

- Intégration de données géographiques
- Statistiques géographiques (qu'est-ce qui est planté quand et où ?)
- Partage d'agencements fonctionnels (tel plant marche bien avec tel plant, plantés de telle manière)
- Intégration de données météorologiques
- Sauvegarde de photos des plants
- Partage de données inter-instances : bibliothèque de plantes, de stats, etc... (ActivityPub ?)

## Écosystème

Si on peut parler d'écosystème, on a [une instance qui tourne](https://garden-party.experimentslabs.com), un semblant de [manuel utilisateur](https://doc.garden-party.experimentslabs.com) et ce blog. Ah, et un [chat matrix](https://matrix.to/#/!fROsPDUgtYILajhMWg:matrix.org?via=matrix.org), et une [liste de diffusion](https://framalistes.org/sympa/subscribe/garden-party).

## Vous voulez participer ?

- Vous êtes dev VueJS et que vous aimez écrire des tests, bah... plus que bienvenue, on en manque cruellement (tests avec `vue-test-utils` et `Jest`)
- Vous avez utilisé l'application mais que l'interface vous rebute, on en discute, elle répond au besoin de ses devs pour le moment... Ce qui est très bien, mais pas assez pour être utilisé par d'autres.
- Les pictos d'éléments sont générés (SVG) et peu différenciables : un _Poirier_ et un _Pommier_ auront exactement le même picto (même _niveau_, même première lettre). Ce serait top d'avoir des pictos par famille (forme par famille, lettre par élémént, par exemple).
- Vous voulez plus de contenu dans la bibliothèque, proposez !
- Heu... On est des quiches en jardinage... les termes ne collent pas ? On veut apprendre ! Dites nous tout !
- Il manque des actions...

Si vous voulez nous dire merci, c'est gentil.

Et si vous nous dites que c'est tout pourri, sans explication... &rarr; `/dev/null`

## Liens utiles

- [Sources de Garden Party](https://gitlab.com/experimentslabs/garden-party/)
- [Sources du manuel](https://gitlab.com/experimentslabs/garden-party-docs)

Une nouvelle aventure commence !
