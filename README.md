# Garden Party - Blog

> Blog around Garden party

## Pre-requisites

To build locally, you'll need:

- `imagemagick` (for `convert`)
- `webp`

## Local build

1. Clone the repository
2. `bundle install`
3. preview with `jekyll serve`
3. `jekyll build`
