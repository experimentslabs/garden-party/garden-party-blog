---
layout: page
title: À propos
permalink: /a-propos/
---

Ce blog a démarré en conjonction avec le développement de [Garden Party](https://garden-party.experimentslabs.com), une application d'aide à la gestion de jardin destinée aux débutants.

Le but de ce blog, hormis raconter une (belle ?) histoire, est de coupler les notes de développement de l'application et son utilisation dans un cas réel, afin d'en expliquer les orientations et son fonctionnement.

Plus d'informations sur Garden Party :

- [Première instance](https://garden-party.experimentslabs.com)
- [Code source](https://gitlab.com/experimentslabs/garden-party) - Licence MIT
- [Manuel utilisateur](https://doc.garden-party.experimentslabs.com/)
- [Chat de développement](https://matrix.to/#/!fROsPDUgtYILajhMWg:matrix.org?via=matrix.org) (Matrix)
- [Liste de diffusion](https://framalistes.org/sympa/subscribe/garden-party) (Email)
