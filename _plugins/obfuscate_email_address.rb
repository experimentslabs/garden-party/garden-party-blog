require 'base64'

# Shamelessly copied from https://gist.github.com/patrickfav/3f9127e25dd6538f0d682b89cbfaefd9

module ObfuscateMailAddress
  def obfuscate_email(input)
    base64_mail = Base64.strict_encode64 input

    # See http://techblog.tilllate.com/2008/07/20/ten-methods-to-obfuscate-e-mail-addresses-compared/
    <<~HTML
      <a href="#" data-contact="#{base64_mail}" target="_blank" onfocus="this.href = 'mailto:' + atob(this.dataset.contact)">
        <script type="text/javascript">document.write(atob("#{base64_mail}"))</script>
      </a>
    HTML
  end
end

Liquid::Template.register_filter(ObfuscateMailAddress)
